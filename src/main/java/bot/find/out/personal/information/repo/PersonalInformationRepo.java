package bot.find.out.personal.information.repo;

import bot.find.out.personal.information.entity.User;

public interface PersonalInformationRepo {
    void addUser(User user);
    void updateUser(User user);
    User selectUser(long chatId);
}
