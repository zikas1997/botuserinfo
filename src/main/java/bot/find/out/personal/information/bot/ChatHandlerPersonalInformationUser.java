package bot.find.out.personal.information.bot;

import bot.find.out.personal.information.entity.User;
import bot.find.out.personal.information.repo.UserPersonalInformationRepository;

import java.util.ArrayList;
import java.util.List;

public class ChatHandlerPersonalInformationUser {

    //-----------------------------------------------------------

    private UserPersonalInformationRepository repository = new UserPersonalInformationRepository();
    private String messageText;
    private long chatId;

    //-----------------------------------------------------------

    public ChatHandlerPersonalInformationUser() {}

    //-----------------------------------------------------------

    //Метод выбирает полходящий ответ, в рамках своей работы.
    public void choiceOfAnswerAndSaveInformation(User user){

        //Условия расположены в обратном порядке.

        chatId = user.getChatId();

        //Благодарим пользователя за опрос
        if(repository.selectUser(user.getChatId()) != null && repository.selectUser(user.getChatId()).getLastMessageBot().equals("Балгодарим за пройденый опросник. Если появиться новая информация я сообщу вам!")){
            messageText = "Балгодарим за пройденый опросник. Если появиться новая информация я сообщу вам!";
        }

        //Узнаем телефон пользователя и сохраняем.
        if(repository.selectUser(user.getChatId()) != null && repository.selectUser(user.getChatId()).getLastMessageBot().equals("Напишите свой номер телефона?" )){
            messageText = "Балгодарим за пройденый опросник. Если появиться новая информация я сообщу вам!";
            user.setLastMessageBot(messageText);
            user.setNumberPhone(user.getLastMessageUser());
            repository.updateUser(user);
        }

        //Узнаем имя пользователя и сохраняем.
        if(repository.selectUser(user.getChatId()) != null && repository.selectUser(user.getChatId()).getLastMessageBot().equals("Скажите свое имя?" )){
            messageText = "Напишите свой номер телефона?";
            user.setLastMessageBot(messageText);
            user.setName(user.getLastMessageUser());
            repository.updateUser(user);
        }

        //Узнаем фамилию пользователя и сохраняем.
        if(repository.selectUser(user.getChatId()) != null && repository.selectUser(user.getChatId()).getLastMessageBot().equals("Скажите свою фамилию?" )){
            messageText = "Скажите свое имя?";
            user.setLastMessageBot(messageText);
            user.setSurname(user.getLastMessageUser());
            repository.updateUser(user);
        }

        //Проверяеться ответ пользоватееля, если согласен начинаем порос, иначе задаем впопрс заного.
        if(repository.selectUser(user.getChatId()) != null &&
                (repository.selectUser(user.getChatId()).getLastMessageBot().equals("Здраствуйте, я добрый бот! Не хотите ли поделиться своими личными данными? Ответьти да, если согланы.")
                        || repository.selectUser(user.getChatId()).getLastMessageBot().equals("Жаль, но может вы бы не отказались ими поделиться?"))){
            if(user.getLastMessageUser().equals("да")|| user.getLastMessageUser().equals("Да")){
                messageText = "Скажите свою фамилию?";
                user.setLastMessageBot(messageText);
                repository.updateUser(user);
            }else {
                messageText = "Жаль, но может вы бы не отказались ими поделиться?";
                user.setLastMessageBot(messageText);
                repository.updateUser(user);
            }
        }

        //Пользователь 1 раз обратился к боту
        if(repository.selectUser(user.getChatId()) == null){
            messageText = "Здраствуйте, я добрый бот! Не хотите ли поделиться своими личными данными? Ответьти да, если согланы.";
            user.setLastMessageBot(messageText);
            repository.addUser(user);
        }
    }

    public String getMessageReply() {
        return messageText;
    }

    public long getChatId() {
        return chatId;
    }
}
