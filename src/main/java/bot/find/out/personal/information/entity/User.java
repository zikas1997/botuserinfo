package bot.find.out.personal.information.entity;

public class User {

    private long chatId;
    private String name;
    private String surname;
    private String numberPhone;
    private String lastMessageBot;
    private String lastMessageUser;


    public User() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getNumberPhone() {
        return numberPhone;
    }

    public void setNumberPhone(String numberPhone) {
        this.numberPhone = numberPhone;
    }

    public long getChatId() {
        return chatId;
    }

    public void setChatId(long chatId) {
        this.chatId = chatId;
    }

    public String getLastMessageBot() {
        return lastMessageBot;
    }

    public void setLastMessageBot(String lastMessageBot) {
        this.lastMessageBot = lastMessageBot;
    }

    public String getLastMessageUser() {
        return lastMessageUser;
    }

    public void setLastMessageUser(String lastMessageUser) {
        this.lastMessageUser = lastMessageUser;
    }

    public void clone(User user){
        chatId = user.getChatId();
        name = user.getName();
        surname = user.getSurname();
        numberPhone = user.getNumberPhone();
        lastMessageBot = user.getLastMessageBot();
        lastMessageUser = user.getLastMessageUser();
    }
}
